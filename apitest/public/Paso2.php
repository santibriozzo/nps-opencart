<?php

if(empty($_SERVER['HTTP_REFERER'])) header("Location: Paso1.php");
//if(substr($_SERVER['HTTP_REFERER'],-5,1) != 1) header("Location: Paso1.php");

session_start();
include_once(dirname(__FILE__).'/../lib/Sub1/functions.php');
require_once(dirname(__FILE__).'/../lib/Sub1/psp_log.php');
$config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');

$_SESSION['psp_PurchaseDescription'] = isset($_REQUEST['psp_PurchaseDescription']) ? $_REQUEST['psp_PurchaseDescription'] : '';
$_SESSION['psp_Amount'] = isset($_REQUEST['psp_Amount']) ? $_REQUEST['psp_Amount'] : '';


psp_log('PASO2: El usuario selecciona el metodo de pago');




?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<title>NPS :: MODELO 3 PARTES :: PASO 2</title>
<script>
function getChecked(element) {
    len = element.length
    for (i = 0; i <len; i++) {
        if (element[i].checked) {
            return element[i].value
        }
    }
    return '';
}        
</script>

</head>
<body>
<h1>MODELO 3 PARTES (<?php echo isset($_SESSION['server']) ? $_SESSION['server'] : 'default' ?>)</h1>

<h2>2. El usuario se dispone a abonar el valor del producto, y elige uno de los medios de pago habilitados.</h2>

<form action="Paso3.php" method="POST">
<table border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td>Seleccionar medio de pago</td>
    </tr>
    <tr>
        <td>
            <input type="radio" name="psp_Product" value="14"  />VISA
            <input type="radio" name="psp_Product" value="5"  />MASTERCARD
            <input type="radio" name="psp_Product" value="1"  />AMERICAN EXPRESS
            <input type="radio" name="psp_Product" value="2"  />DINERS
        </td>
    </tr>
    <tr>
        <td><input type="radio" name="psp_Product" value="320"  />PAGOMISCUENTAS.COM</td>
    </tr>
    <tr>
        <td>
            <input type="radio" name="psp_Product" value="300"  />RAPIPAGO
            <input type="radio" name="psp_Product" value="301"  />PAGOFACIL
            <input type="radio" name="psp_Product" value="302"  />BAPROPAGOS
        </td>
    </tr>    
</table>
<br />
<input type="button" onclick="(getChecked(document.forms[0].elements['psp_Product']) ? document.forms[0].submit() : alert('Seleccione un metodo de pago para continuar con la compra'))" value="Continuar" />
</form>


</body>
</html>