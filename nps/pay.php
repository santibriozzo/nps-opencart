<?php
  require_once(DIRNAME(__FILE__) . '/lib/Sub1/psp_client.php');
  require_once(DIRNAME(__FILE__) . '/lib/Sub1/functions.php');


  function nps_pay($data, $history_id) {
	$psp_parameters = array(
		'psp_Version'                  => '1',
		'psp_MerchantId'               => 'verona',
		'psp_TxSource'                 => 'WEB',
		'psp_MerchTxRef'               => $data['id'] . '-'. $history_id,
		'psp_PosDateTime'              => date('Y-m-d H:i:s'),
		'psp_MerchOrderId'             => $data['id'],
		'psp_Currency'                 => '032',
		'psp_CustomerMail'             =>  $data["titular"]["email"],
		'psp_MerchantMail'             => 'kevlugli@gmail.com',
		'psp_Product'                  =>  $data["metodo"], // METODO DE PAGO. 14=VISA
		'psp_Country'                  => 'ARG',
		'psp_ReturnURL'                => 'https://www.neumaticosverona.com.ar/nps/confirm.php',
		'psp_FrmLanguage'              => 'es_AR',
		'psp_FrmBackButtonURL'         => 'https://www.neumaticosverona.com.ar/nps/index.php',
		 //----- ACA TERMINAN DATOS BASICOS PARA TODOS LOS PROCESOS -----
		 //----- ACA COMIENZAN LOS DATOS PARA EL METODO PayOnLine_3p ----
		 'psp_Amount'                  =>  $data['total'],
		 'psp_NumPayments'             =>  $data['cuotas'],
		 'psp_PurchaseDescription'     =>  $data['descripcion']
	  );
	  
	  if (isset($data['promo'])) {
		$psp_parameters['psp_PromotionCode'] = $data['promo'];
	  }

	  $cli = new PSP_Client();
	  $cli->setDebug(false);
	  $cli->setPrintRequest(false);
	  $cli->setPrintResponse(false);
	  $cli->setConnectTimeout(20);
	  $cli->setExecuteTimeout(40);
	  
	  //$cli->setUrl('https://implementacion.nps.com.ar/ws.php?wsdl');
	  $cli->setUrl('https://services2.nps.com.ar/ws.php?wsdl');
	  
	  $cli->setWsdlCache(DIRNAME(__FILE__) . '/cache/', 0);
	  
	  //$cli->setSecretKey('zgeJgGoa5lSN1Wmy469a9k3vQLrtBGi9uMQabFAe0azz7lx9q5hzpitD4k6DymKt');
	  $cli->setSecretKey('ObsiT0JBuiIkdcIDjDO2QytGyoew1wwPVp6ypPAi6QQj3gwktKnKGeYDQC7R94oe');
	  
	  $cli->setMethodName('PayOnLine_3p');
	  $cli->setMethodParams($psp_parameters);
	  $result = $cli->send();
	  //var_dump($result);
	  if ($result['psp_ResponseCod'] == 1 && isset($result['psp_Session3p']) && $result['psp_Session3p'] != '') {
		$resp = array(
			'action'			=> $result['psp_FrontPSP_URL'],
			'session'			=> $result['psp_Session3p'],
			'txRef'				=> $result['psp_MerchTxRef'],
			'merchantId'		=> $result['psp_MerchantId'],
			'transactionId'		=> $result['psp_TransactionId']
		);
		
		return json_encode($resp);
	  }  else {
		
		return $result['psp_ResponseExtended'];
	  }
  }
  
  function nps_confirm($id) {
	  $psp_parameters = array(
		'psp_Version'                  => '1',
		'psp_MerchantId'               => 'verona',
		'psp_QueryCriteria'			   => 'M',
		'psp_QueryCriteriaId'		   => $id,
		'psp_PosDateTime'			   => date('Y-m-d H:i:s')
	  );

	  $cli = new PSP_Client();
	  $cli->setDebug(false);
	  $cli->setPrintRequest(false);
	  $cli->setPrintResponse(false);
	  $cli->setConnectTimeout(20);
	  $cli->setExecuteTimeout(40);
	  
	  //$cli->setUrl('https://implementacion.nps.com.ar/ws.php?wsdl');
	  $cli->setUrl('https://services2.nps.com.ar/ws.php?wsdl');
	  
	  $cli->setWsdlCache(DIRNAME(__FILE__) . '/cache/', 0);
	  
	  //$cli->setSecretKey('zgeJgGoa5lSN1Wmy469a9k3vQLrtBGi9uMQabFAe0azz7lx9q5hzpitD4k6DymKt');
	  $cli->setSecretKey('ObsiT0JBuiIkdcIDjDO2QytGyoew1wwPVp6ypPAi6QQj3gwktKnKGeYDQC7R94oe');
	  
	  $cli->setMethodName('SimpleQueryTx');
	  $cli->setMethodParams($psp_parameters);
	  $result = $cli->send();
	  
	  if (isset($result['psp_Transaction']) && $result['psp_Transaction']['psp_ResponseCod'] == 0) {
		  return true;
	  } else {
		  return false;
	  }
	  
  }
  
?>
