<?php
require_once(getcwd() . '/nps/pay.php');

class ControllerExtensionPaymentNps extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/extension/payment/nps')) {
			return $this->load->view($this->config->get('config_template') . '/extension/payment/nps', $data);
		} else {
			return $this->load->view('default/template/extension/payment/nps', $data);
		}
	}
	
	public function form() {
		
		$this->load->model('extension/payment/nps');
		$this->load->model('catalog/information');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->document->setTitle('Pagar con tarjeta de crédito');
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'extension/payment/nps_form')) {
			echo $this->load->view($this->config->get('config_template') . 'extension/payment/nps_form', $data);
		}	
		 else {
			echo $this->load->view('extension/payment/nps_form', $data);	
		}
}


	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'nps') {
			  $order_id = $this->session->data['order_id'];
			  $history_id = $this->session->data['history_id'];
			  
			  if (nps_confirm($order_id . '-' . $history_id)) {
				$this->load->model('checkout/order');
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_nps_order_status_id'), false);
				
				header('Location: '.$this->url->link('checkout/success'));
				die();
			  } else {
				header('Location: /index.php?route=payment/nps/form&error=true');  
				die();
			  }
			  
			  

		}
	}
	public function getData() {
		$customer_email = $this->customer->getEmail();
		$all_products = $this->cart->getProducts();		
		$items = array();
	
		foreach ($all_products as $product) {
			array_push($items, array('descripcion' => $product['name'] . ' x ' . $product['quantity'], 'precio' => $product['total']));
		}
		
		$cart_data = array(
			'email' => $customer_email, 
			'items' => $items, 
			'id' => $this->session->data['order_id']
		);
		
		echo json_encode($cart_data);
		
	}
	
	public function pay() {
		$this->load->model('checkout/order');
		$this->load->model('extension/payment/nps');

		
		$order_id = $this->session->data['order_id'];
		
		$order_status = $this->model_checkout_order->getOrder($order_id)['order_status_id'];
		
		$this->model_checkout_order->addOrderHistory($order_id, $order_status, 'NPS TX Init', false);
			  
		$history_id = $this->model_payment_nps->getLastOrderHistoryId($order_id);
		
		$this->session->data['history_id'] = $history_id;
		
		$data = json_decode(file_get_contents('php://input'), true);
		
		echo nps_pay($data, $history_id);
		
	}
}
