<?php
class ModelExtensionPaymentNps extends Model {
	 public function getLastOrderHistoryId($order_id) {
		$query = $this->db->query("SELECT order_history_id FROM " . DB_PREFIX . "order_history WHERE order_id = " . $order_id . " ORDER BY order_history_id DESC LIMIT 1");
		foreach ($query->rows as $item) {
			return $item['order_history_id'];
	    }
	} 
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/nps');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_nps_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('payment_nps_total') > 0 && $this->config->get('payment_nps_total') > $total) {
			$status = false;
		} else if (!$this->config->get('payment_nps_geo_zone_id')) {
			$status = true;
		} else if ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		//$emails = array(
			//'kevlugli@gmail.com', 
			//'valdiviesoharald@gmail.com', 
			//'ventas@neumaticosverona.com.ar'
		//);
		
		//$email = $this->customer->getEmail();
		//if (!in_array($email, $emails)) {
			//$status = false;
		//}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'nps',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('payment_nps_sort_order')
			);
		}

		return $method_data;
	}
}
