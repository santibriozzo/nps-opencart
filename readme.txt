NPS OPENCART 3
----------------
Nps estable para la version 3 de opencart.
Para utilizarlo en la version 2 de open cart, es necesario cambiar forma en 
la que llama a cada uno de los archivos dentro de controller, como asi
tambien las diferentes variables, y sintaxis en las vistas.


Pasos para instalar

1.- Copiar todas las carpetas, luego pegarlas en la raiz del proyecto
2.- En cada uno de los archivos se recomienda revisar y cambiar las rutas
de los mismos 
archivos a controlar rutas:
	-CONTROLLER (AMBOS-admin y catalog-).
	-VIEW (AMBOS-admin y catalog-).
	-MODEL (AMBOS-admin y catalog-).
tambien se debe revisar y cambiar si es necesario las rutas de los 
diferentes archivos .js :
.Carpeta NPS2 ->   api.js, app.js, vendor.js
.Carpeta NPS -> main.js

---------------------------
POSIBLES ERRORES
-Error de constructores y posible problemas en versiones posteriores de php
el error estas en los contructores de NUSOAP,por ej:
 	class nusoap_base ----- se deberia cambiar por: class __nusoap_base
Como asi tambien en los diferentes constructores que se encuentran en el archivo nusoap.php

