<?php

/* getPSPMethodByPSPProduct
 * Look for the method name for the PSP_Product 3p given
 * @param value number
 * @return string
 */
function getPSPMethodByPSPProduct($value){
    $method_name = '';
    
    switch($value)
    {
      case '14':
      case '5':    
      case '1':        
      case '2':
          $method_name = 'PayOnLine_3p';
          break;
      case '320':
          $method_name = 'BankPayment_3p';
          break;
      case '300':
      case '301':
      case '302':    
          $method_name = 'CashPayment_3p';
          break;  
    }
    
    return $method_name;
}

/* getClientConfigAsArray
 * populate array of configuration vars from the config.xml file
 * @return array
 */
function getClientConfigAsArray() {
     $s = file_get_contents(dirname(__FILE__)."/../../config/config.xml");
     $a = json_decode(json_encode((array) simplexml_load_string($s)),1);
     return $a;
}


/* isConfigFileComplete
 * check if the config.xml file is 100% completed
 * @return boolean
 */
function isConfigFileComplete() {
   foreach(getClientConfigAsArray() as $item) {
     if($item == NULL || $item == '') return false;
   }
   return true;
}

/* isCardsFileComplete
 * check if the config.xml file is 100% completed
 * @return boolean
 */
function isCardsFileComplete() {
   foreach(getConfigCardInsterestsAsArray() as $item) {
     var_dump($item);  
       
   }
   return true;
}

/* getInvoiceNumber
 * make random invoice number for testing porpuse
 * @return string
 */
function getInvoiceNumber() {
    return 'A-0000'.rand(100000,99999);
}

/* calculateDaysUntilSecondExpDate
 * get numbers of days between first and the second exp date for testing porpuse
 * @return number
 */
function calculateDaysUntilSecondExpDate() {
    // secondExpDate - psp_FirstExpDate
    return 15;
}

/* calculateDaysAvailableToPay
 * get numbers of days between current date and second exp date for testing porpuse
 * @return number
 */
function calculateDaysAvailableToPay() {
    // secondExpDate - current_date()
    return 0;
}

/* render3pForm
 * create html form by given $result for NPS submition
 * @param result array [array of result from the PayOnline_3p method]
 * @param result $force_send [auto-submition]
 * @return string
 */
function render3pForm($result,$force_send=false) {
    $html = <<<BALBEFSD

    <form name=form093285098 action="{$result['psp_FrontPSP_URL']}" method="POST">
    <table>
    <tr>
        <td>psp_Session3p</td>
        <td><input type="text" name="psp_Session3p" value="{$result['psp_Session3p']}"></td>
    </tr>
    <tr>
        <td>psp_TransactionId</td>
        <td><input type="text" name="psp_TransactionId" value="{$result['psp_TransactionId']}"></td>
    </tr>
    <tr>
        <td>psp_MerchantId</td>
        <td><input type="text" name="psp_MerchantId" value="{$result['psp_MerchantId']}"></td>
    </tr>     
    <tr>
        <td>psp_MerchTxRef</td>
        <td><input type="text" name="psp_MerchTxRef" value="{$result['psp_MerchTxRef']}"></td>
    </tr>     
    </table>
    <input type="submit" value="Enviar">
    </form>

BALBEFSD;
        
  if($force_send === true)$html .= '<script>document.forms[\'form093285098\'].submit()</script>';
        
        
  return $html;
}

/* checkWSConnection
 * check connectivity to WSDL
 * @return boolean
 */
function checkWSConnection(){
    $config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');
    include_once(dirname(__FILE__).'/../NuSoap/nusoap.php');
    try {
        $client = new soapclient($config['ws_url']);
    }catch(Exception $e) {
        return false;
    }
    return true;  
}

/* calculateFeesByIdProductAndAmount
 * get fees amount and total
 * @param $psp_product integer
 * @param $psp_amount float
 * @return array
 */
function calculateFeesByIdProductAndAmount($psp_product, $psp_amount){
  $nodes = getConfigCardInsterestsAsArray();
  $amount = number_format($psp_amount/100,2);
  $fees = array();  
  foreach($nodes as $cards) {
      foreach($cards as $card) {
          if($card['psp_product'] == $psp_product) {
            $options = count($card['payments']['payment']);
            $i = 0;
            foreach($card['payments']['payment'] as $fee) {
                $i++;
                    if( (float)$fee['interest'] > 0 ) {
                      $fee['amount'] = number_format(($amount+($amount*$fee['interest']/100))/$fee['quantity'],2);
                    }else {
                      $fee['amount'] = number_format($amount/$fee['quantity'],2);
                    }
                    $fee['total'] = number_format($fee['amount']*$fee['quantity'],2);
                    array_push($fees,$fee);
                    
                    
                    
            }
          }
      }
  }  
  return $fees;
}

/* getConfigCardInsterestsAsArray
 * populate array of configuration vars from the card_interest.xml file
 * @return array
 */
function getConfigCardInsterestsAsArray() {
     $s = file_get_contents(dirname(__FILE__)."/../../config/card_interest.xml");
     $a = json_decode(json_encode((array) simplexml_load_string($s)),1);
     return $a;
}

/* getTotalAmountByFee
 * retrieve the total amount
 * @param $quantity integer
 * @param $psp_product integer
 * @param $psp_amount float
 * @return float || false
 */
function getTotalAmountByFee($quantity,$psp_product, $psp_amount) {
    $fees = calculateFeesByIdProductAndAmount($psp_product, $psp_amount);
    foreach($fees as $fee) {
      if($fee['quantity'] == $quantity) {
        if((float)$fee['interest'] > 0) {
          return $fee['total'];
        }else {
          return number_format($psp_amount/100,2);
        }
      }  
    }
    return false;
}

/* getPSPAmount
 * retrieve the total amount with PSP param format
 * @param $quantity integer
 * @param $psp_product integer
 * @param $psp_amount float
 * @return integer [formated $$$$cc]
 */
function getPSPAmount($quantity,$psp_product, $psp_amount) {
    return getTotalAmountByFee($quantity,$psp_product, $psp_amount)*100;
}

/* getFeesByPSPProduct
 * return active fees for the product given
 * @param $psp_product integer
 * @return array
 */
function getFeesByPSPProduct($psp_product) {
    $nodes = getConfigCardInsterestsAsArray();
    $fees = array();  
    foreach($nodes as $cards) {
        foreach($cards as $card) {
            if($card['psp_product'] == $psp_product) {
                $fees = $card['payments']['payment'];
            }
        }
    }
    return $fees;
}

/* getServers
 * retrieve all servers on the configuration file
 * @return array
 */
function getServers(){
    $config = getClientConfigAsArray();
    return $config['server'];
}

/* getConfigsByServerName
 * fetch server configuracion from config.xml by server name
 * @param server_name string
 * @return array || false
 */
function getConfigsByServerName($server_name) {
    $config = getClientConfigAsArray();
    
    // 1 server configured only
    if(isset($config['server']['name']) && $config['server']['name'] == $server_name) return $config['server'];
    
    // more than 1 server configured
    if(isset($config['server'][0]) && is_array($config['server'][0])) {
        foreach($config['server'] as $server) {
            if($server['name'] == $server_name) return $server;
        }        
    }
    
    return false;
}