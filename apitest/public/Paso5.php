<?php

session_start();

require_once(dirname(__FILE__).'/../lib/Sub1/psp_client.php');
require_once(dirname(__FILE__).'/../lib/Sub1/psp_log.php');
require_once(dirname(__FILE__).'/../lib/Sub1/functions.php');

$config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');


?>

<html>
<head>
<title>NPS :: MODELO 3 PARTES :: PASO 5</title>    
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<h1>MODELO 3 PARTES (<?php echo isset($_SESSION['server']) ? $_SESSION['server'] : 'default' ?>)</h1>

<h2>5. El usuario complet&oacute; el formulario de pago, y el servidor PSP lo redireccion&oacute; a la p&aacute;gina configurada.</h2>

<h3>Resultado de la operaci&oacuten:</h3>



<?php



  psp_log('PASO5: el cliente consulta via ws el estado de la transaccion ('.$_POST['psp_TransactionId'].')');

// SimpleQueryTx
$psp_parameters_query = array(
    'psp_Version'         => $config['psp_Version'],
    'psp_MerchantId'      => $_SESSION['psp_MerchantId'],
    'psp_QueryCriteria'   => 'T',
    'psp_QueryCriteriaId' => $_POST['psp_TransactionId'],
    'psp_PosDateTime'     => date('Y-m-d H:i:s')	
);

try{
  $cli = new PSP_Client();
  $cli->setDebug(false);
  $cli->setPrintRequest(false);
  $cli->setPrintResponse(false);
  $cli->setConnectTimeout(20);
  $cli->setExecuteTimeout(40);
  
  $cli->setUrl($config['ws_url']);

  $cli->setWsdlCache('../cache/', 0);

  $cli->setSecretKey($_SESSION['secret_key']);
  
  $cli->setMethodName('SimpleQueryTx');
  $cli->setMethodParams($psp_parameters_query);
  $result = $cli->send();

  if($result['psp_ResponseCod'] == 2) { //[respuesta ok de la consulta con el id de transaccion]
      
      switch($result['psp_Transaction']['psp_ResponseCod']){  
          case 0: // [indica que la transaccion fue aprobada]
              echo "El pago fue aprobado";
              break;
          case 16:
              echo "Pendiente de pago - Cupon de pago emitido";
              break;          
          case 18:
              echo "Pendiente de pago - Alta de factura y Alta de Adhesion efectuadas";
              break;             
          default:
              echo $result['psp_Transaction']['psp_ResponseMsg'];
              break;
          
      }
      
  }else {
      echo $result['psp_Transaction']['psp_ResponseMsg'];
  }
  
  
  
  echo '<h2>Result</h2><pre>';
  print_r($result);
  echo '</pre>';         
  
}
catch (Exception $e)
{
    psp_log('ERROR '.$e->getCode().' - '.$e->getMessage());    
    
    
  echo '<h2>Result</h2><pre>';
  echo 'Error Cod: ' . $e->getCode() . '<br>';
  echo 'Error Msg: ' . $e->getMessage() . '<br>';
  echo '</pre>';                       
}

?>

<br /><br />
<a href="Paso1.php">Volver al primer paso</a>

</body>
</html>