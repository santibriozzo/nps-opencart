<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pago electrónico</title>
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="/nps/css/spectre.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> 
  </head>
  <body style="font-family: lato; background: #eee;">
    <div id="app">
      <div v-if="cargando">
        <h2 style="text-align: center;"> Te estamos enviando al proceso de Pago... Por favor espera un instante</h2>
      </div>
      <div v-else class="container">
        <h3 style=" padding-bottom: 2em; padding-top: 2em; font-weight: bold; font-size: 25px; color: #777; font-family: lato; text-transform: none; ">¡Ya casi estás! Completá tus datos y finalizá tu compra</h3>
        <div class="columns">
			<div class="column col-8">
				<div style="background-color: rgb(255, 255, 255);padding-top: 1em;padding-bottom: 4em;border-radius: 21px;">
					<h5 style="color: #292929;text-transform: none;padding-left: 1em;font-weight: bold;">Titular tarjeta</h5>
					<hr>
					<div class="columns">
						<div class="column col-4" style="padding-left: 35px;">
						  <label class="form-label" :style="nombreCompletado? '' : 'color:red;'">Nombre y apellido *</label>
						  <input type="text" v-model="pedido.titular.nombre" name="nombre" class="form-input"></input>
						</div>
						<div class="column col-4" style="padding-left: 2px; ">
						  <label class="form-label" :style="dniCompletado? '' : 'color:red;'">Documento *</label>
						  <input type="text" v-model="pedido.titular.dni" name="dni" class="form-input"></input>
						</div>
						<div class="column col-4" style="padding-left: 2px;">
						  <label class="form-label" :style="correoCompletado? '' : 'color:red;'">Correo electrónico *</label>
						  <input type="text" v-model="pedido.titular.correo" name="email" class="form-input"></input>
						</div>
					</div>
				</div>
				<div style="padding: 1em">
					<h5>¿Cómo querés pagar?</h5>
					<table class="table table-striped table-hover" style="border: solid 1px #ddd8d8; border-radius: 40px;">
						<tbody>
							<tr v-for="(forma, index) in formasDePago">
								<td>
									<label class="form-radio">
										<input type="radio" :value="index" v-model="pedido.metodo" @change="pedido.tarjeta = formasDePago[index].tarjetas[0]" />
										<i class="form-icon" style="width: 2.5rem; height: 2.5rem; border-radius: 2.5rem; "></i> 
										<span style="margin-left: 2em;">
											<span style="font-size: 3em;">{{forma.strong}}</span>
											<span>{{forma.text}}</span>
											<span class="label label-success" v-if="forma.label">{{forma.label}}</span>
										</span>
									</label>
								</td>
								<td>
									<img v-for="tarjeta in forma.tarjetas" :src="'img/tarjetas/' + tarjeta + '.png'" style="width: 4em; heigth: 2.5em;"/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div style="background-color: #F2F2F2; padding: 1em;">
					<h5 style="color: blue;">Seleccione tarjeta</h5>
					<hr>
					<table class="table table-striped table-hover">
						<tbody>
							<tr v-for="tarjeta in formasDePago[pedido.metodo].tarjetas">
								<td>
									<label class="form-radio">
										<input type="radio" :value="tarjeta" v-model="pedido.tarjeta" />
										<i class="form-icon" style="width: 2.5rem; height: 2.5rem; border-radius: 2.5rem; "></i> 
										<span style="margin-left: 2em;">
											<span style="font-size: 2em;">{{tarjetas[tarjeta].descripcion}}</span>
										</span>
									</label>
								</td>
								<td>
									{{formasDePago[pedido.metodo].cuotas}} cuotas de ${{calcularInteres(tarjeta, formasDePago[pedido.metodo].cuotas).cuota}}<br>
									Total: ${{calcularInteres(tarjeta, formasDePago[pedido.metodo].cuotas).total}}
								</td>
								<td>
									<img :src="'img/tarjetas/' + tarjeta + '.png'" style="width: 5em; heigth: 3em;"/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="column col-4">
				<div style="background-color: rgb(255, 255, 255);padding: 1em;border-radius: 12px;">
					<h5 style="color: black;text-transform: none;">Detalle del pago</h5>
					<div class="columns" v-for="item in pedido.items" style="padding-top: 2em;padding-bottom: 2em;font-family: lato;padding-right: 5px;padding-left: 5px;font-size: 12px;border-bottom: solid 1px #e5e0e0;">
						<div class="column col-9">
							{{item.descripcion}}
						</div>
						<div class="column col-3">
							${{item.precio}}
						</div>
					 </div>
					 <div style="text-align: right;padding-right: 5px;font-size: 18px;font-family: inherit;padding-top: 25px;text-transform: uppercase;">
						 Total <span style="margin-left: 10em;font-size: 20px;">${{total}}</span>
					 </div>
				</div>
			</div>
		</div>
		<div style="padding:1em;">
			<button 
			  style="font-size: 1.3em;"
			  @click="siguientePaso()" 
			  class="btn btn-primary"
			  :disabled="datosCompletados? false: true">
			  Siguiente
			</button>
			<span v-if="!datosCompletados">* Debe completar los datos antes de continuar</span>
		</div>
        
      </div>
      <div style="height: 0px; overflow: hidden;">
		<form :action="nps.action" :name="nps.name"  method="POST">
			<input type="text" name="psp_Session3p" :value="nps.session">
			<input type="text" name="psp_TransactionId" :value="nps.transactionId">
			<input type="text" name="psp_MerchantId" :value="nps.merchantId">
			<input type="text" name="psp_MerchTxRef" :value="nps.txRef">
			<button type="sumbit" :id="nps.buttonId"></button>
		</form>
      </div>
      <div class="modal active" v-for="modal in modals">
		  <div class="modal-overlay"></div>
		  <div @click="modal.cancela()" class="modal-container">
			<div class="modal-header">
			  <button  @click="modal.cancela()"  class="btn btn-clear float-right"></button>
			  <div class="modal-title">{{modal.title}}</div>
			</div>
			<div class="modal-body">
			  <div class="content" v-html="modal.contenido">
			  </div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-primary" @click="modal.exito()">Aceptar</button>
			</div>
		  </div>
		</div>

    </div>
    <script src="/nps/main.js"></script>
  </body>
</html>
