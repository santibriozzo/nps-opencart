<?php

session_start();
include_once(dirname(__FILE__).'/../lib/Sub1/functions.php');
require_once(dirname(__FILE__).'/../lib/Sub1/psp_log.php');



// load server configuration
$config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');

// load session vars for ws parameters
$_SESSION['psp_CustomerMail'] = 'customer@psp.com.ar';
if($_POST) {
    $_SESSION['psp_MerchantId'] = isset($_REQUEST['psp_MerchantId']) ? $_REQUEST['psp_MerchantId'] : '';
    $_SESSION['secret_key']     = isset($_REQUEST['secret_key']) ? $_REQUEST['secret_key'] : '';
    $_SESSION['psp_Country']    = isset($_REQUEST['psp_Country']) ? $_REQUEST['psp_Country'] : '';
    $_SESSION['psp_Currency']   = isset($_REQUEST['psp_Currency']) ? $_REQUEST['psp_Currency'] : '';
    $_SESSION['psp_MerchantMail']   = isset($_REQUEST['psp_MerchantMail']) ? $_REQUEST['psp_MerchantMail'] : '';
    $_SESSION['psp_MerchantCuil']   = isset($_REQUEST['psp_MerchantCuil']) ? $_REQUEST['psp_MerchantCuil'] : '';
}else {
    $_SESSION['server'] = 'default';
    $config = getConfigsByServerName($_SESSION['server']);    
    $_SESSION['psp_MerchantId'] = $config['psp_MerchantId'];
    $_SESSION['secret_key']     = $config['secret_key'];
    $_SESSION['psp_Country']    = $config['psp_Country'];
    $_SESSION['psp_Currency']   = $config['psp_Currency'];
    $_SESSION['psp_MerchantMail']   = $config['psp_MerchantMail'];
    $_SESSION['psp_MerchantCuil']   = $config['psp_MerchantCuil'];
}

// error_log(date('Y-m-d H:i:s').': PASO1: El usuario compra el producto', 3, '../log/new_api.log');

// log activity
psp_log('PASO1: El usuario compra el producto');

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<title>NPS :: MODELO 3 PARTES :: PASO 1</title>
</head>
<body>
<h1>MODELO 3 PARTES (<?php echo isset($_SESSION['server']) ? $_SESSION['server'] : 'default' ?>)</h1>
<h2>1. Un usuario compra un producto</h2>

<table border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td>Cod</td>
        <td>Descripcion</td>
        <td>Cantidad</td>
        <td>Valor</td>
    </tr>
    <tr>
        <td>LPA10</td>
        <td>Producto1</td>
        <td>1</td>
        <td>$ <?php echo isset($_REQUEST['psp_Amount']) ? number_format($_REQUEST['psp_Amount']/100,2) : '20.00' ?><td>
    </tr>    
</table>
<form action="Paso2.php" method="POST">
    <input type="hidden" name="psp_PurchaseDescription" value="LPA10" />
    <input type="hidden" name="psp_Amount" value="<?php echo isset($_REQUEST['psp_Amount']) ? $_REQUEST['psp_Amount'] : '2000' ?>" />
    <br />
    <input type="submit" value="Comprar" /> 
</form>

</body>
</html>
