<?php

session_start();

require_once(dirname(__FILE__).'/../lib/Sub1/functions.php');

$_REQUEST['psp_product'] = isset($_REQUEST['psp_product']) ? $_REQUEST['psp_product'] : '';
$_REQUEST['psp_amount'] = isset($_REQUEST['psp_amount']) ? $_REQUEST['psp_amount'] : '';

$fees = calculateFeesByIdProductAndAmount($_REQUEST['psp_product'],$_REQUEST['psp_amount']);

?>

<?php if(!empty($fees)): ?>
<table border="0" cellpadding="0" cellspacing="0" width="300" height="400">
    <tr>
        <td>Cuotas</td>
        <td>Interes</td>
        <td>Valor Cta.</td>
    </tr>
    <?php foreach($fees as $fee): ?>
    <tr>
        <td><?php echo $fee['quantity'] ?></td>
        <td><?php echo $fee['interest'] ?>%</td>
        <td>$<?php echo $fee['amount'] ?></td>
    </tr>    
    <?php endforeach ?>
</table>
<?php else: ?>
Lo sentimos, no hay pago en cuotas disponibles para su medio de pago

<?php endif; ?>


