<?php

include_once(dirname(__FILE__).'/../lib/Sub1/functions.php');

?>

<html>
<head>
<title>NPS :: PHP - CLIENT PSP</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<style>
table.tdr {width:50%;}    
table.tdr .rd {border-bottom:1px dotted #000;}

</style>

</head>
<body>

<h1>PHP - CLIENT PSP</h1>

<h2>Revisión de sistema</h2>


<table class="tdr">
    <tr>
        <td class="rd">PHP 5.2 o superior installed</td>
        <td ><?php echo (strnatcmp(phpversion(),'5.2.0') >= 0 ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td></td>
    </tr>
    <tr class="r">
        <td class="rd">CURL extension enabled</td>
        <td><?php echo (extension_loaded('curl') ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td></td>
    </tr>
    <tr class="r">
        <td class="rd">Valid config.xml</td>
        <td><?php echo (isConfigFileComplete() ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td>* Require customize</td>
    </tr>
    <tr class="r">
        <td class="rd">WS connectivity</td>
        <td><?php echo (checkWSConnection() ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td></td>
    </tr>    
    <tr class="r">
        <td class="rd">Writeable cache dir</td>
        <td><?php echo (is_writable('../cache') ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td></td>
    </tr>
    <tr class="r">
        <td class="rd">Writeable api.log</td>
        <td><?php echo (is_writable(dirname(__FILE__).'/../log/api.log') ? '<span style="color:green;">Ok</span>' : '<span style="color:red;">Fail</span>') ?></td>
        <td>* Require writeable file under "log" directory</td>
    </tr>
    
    
    <tr>
        <td colspan="2">
            <input type="button" onclick="document.location.reload()" value="Comprobar"/>
        </td>
    </tr>
</table>

<h3>Demos</h3>
<ul>
    <li><a href="Paso1.php" target="_blank">Modelo 3 partes</a></li>
</ul>

<h3>Logs</h3>
<ul>
    <li><a href="../log/api.log" target="_blank">Api log</a></li>
</ul>

</body>
</html>