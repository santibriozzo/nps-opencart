document.title = "Abonar con tarjeta de crédito"

window.vue = new Vue({
    el: '#app',
    data: {
        modals: [],
        nps: {
            action: '',
            session: '',
            txRef: '',
            transactionId: '',
            merchantId: '',
            name: 'formno',
            buttonId: 'btnno'
        },
        cargando: false,
        pedido: {
            tarjeta: 'visa',
            metodo: 0,
            items: [],
            titular: {
                dni: '',
                nombre: '',
                correo: ''
            }
        },
        tarjetas: {
            debito: {
                descripcion: "VISA Debito",
                codigo: 55,
                cuotas: {
                    1: {
                        coeficiente: 1
                    }
                }
            },
            visa12: {
                descripcion: "VISA",
                codigo: 14,
                cuotas: {},
                promociones: {
                    promoahora12: {
                        cuotas: 12,
                        coeficiente: 1,
                        advertencia: 'Sólo válido de Jueves a Domingo'
                    }
                }
            },
            visa18: {
                descripcion: "VISA",
                codigo: 14,
                cuotas: {},
                promociones: {
                    promoahora18: {
                        cuotas: 18,
                        coeficiente: 1,
                        advertencia: 'Sólo válido de Jueves a Domingo'
                    }
                }
            },
            visa: {
                descripcion: "VISA",
                codigo: 14,
                cuotas: {
                    1: {
                        coeficiente: 1
                    },
                    3: {
                        coeficiente: 1
                    },
                    6: {
                        coeficiente: 1
                    }
                }
            },
            naranja: {
                descripcion: "Naranja",
                codigo: 9,
                cuotas: {
                    12: {
                        coeficiente: 1
                    },
                    14: {
                        coeficiente: 1
                    },
                    18: {
                        coeficiente: 1
                    },
                    24: {
                        coeficiente: 1
                    }
                }
            },
            hsbc: {
                descripcion: "HSBC",
                codigo: 14,
                cuotas: {
                    1: {
                        coeficiente: 1
                    },
                    3: {
                        coeficiente: 1
                    },
                    6: {
                        coeficiente: 1
                    }
                },
                promociones: {
                    /*promohsbc: {
                        cuotas: 6,
                        coeficiente: 1,
                        pre: function(total) {
                            return total / 10 * 9;
                        },
                        advertencia: 'El reintegro se le realizará en el resumen de su tarjeta'
                    }*/
                }
            },
            cabal: {
                descripcion: "Cabal",
                codigo: 8,
                cuotas: {
                    1: {
                        coeficiente: 1
                    },
                    3: {
                        coeficiente: 1
                    },
                    6: {
                        coeficiente: 1
                    }
                },
                promociones: {
                    /*promocabal: {
                        cuotas: 6,
                        coeficiente: 1,
                        pre: function(total) {
                            return total / 10 * 9;
                        },
                        advertencia: 'El reintegro se le realizará en el resumen de su tarjeta'
                    }*/
                }
            },
            bmros: {
                descripcion: "Banco Municipal de Rosario",
                codigo: 14,
                cuotas: {
                    1: {
                        coeficiente: 1
                    },
                    3: {
                        coeficiente: 1
                    },
                    6: {
                        coeficiente: 1
                    }
                },
                promociones: {
                    /*promomunicipal: {
                        cuotas: 3,
                        coeficiente: 1,
                        pre: function(total) {
                            return total / 10 * 9;
                        },
                        advertencia: 'El reintegro se le realizará en el resumen de su tarjeta'
                    }*/
                }
            },
            nbsf: {
                descripcion: "Banco de Santa Fe",
                codigo: 14,
                cuotas: {
                    1: {
                        coeficiente: 1
                    },
                    3: {
                        coeficiente: 1
                    },
                    6: {
                        coeficiente: 1
                    }
                },
                promociones: {
                    promosantafe: {
                        cuotas: 6,
                        coeficiente: 1,
                        pre: function(total) {
                            return total / 100 * 85;
                        },
                        advertencia: 'El reintegro se le realizará en el resumen de su tarjeta'
                    }
                }
            }
        },
        respuesta: ''
    },
    methods: {
        mostrarModal: function(titulo, contenido, exito, cancela) {
            alert(contenido)
            var self = this
            this.modals.push({
                titulo: titulo,
                contenido: contenido,
                exito: function() {
                    if (exito) exito()
                    self.modals.pop()
                },
                cancela: function() {
                    if (cancela) cancela()
                    self.modals.pop()
                }
            })
        },
        errorNPS: function() {
            this.mostrarModal('Error', 'El sistema ha devuelto un error. ¿Tarjeta inválida?')
            /*this.mostrarModal("OK", "Pago registrado, gracias por su compra!");
            console.log("fixear esto, está bypasseado");
                // Your application has indicated there's an error
                window.setTimeout(function(){

                    // Move to a new location or you can do something else
                    window.location.href = "https://www.neumaticosverona.com.ar/index.php?route=payment/nps/confirm";

                }, 5000);*/

        },
        calcularInteres: function(tarjeta, metodo, final) {
            if (!tarjeta)
                return {
                    total: 0,
                    cuota: 0
                }

            var tarjetaObj = this.tarjetas[tarjeta]

            var total = 0
            var cuotas = 0
            if (metodo.cuotas) {
                cuotas = metodo.cuotas
                total = this.total * tarjetaObj.cuotas[cuotas].coeficiente
            } else {
                var promoObj = tarjetaObj.promociones[metodo.promocion[tarjeta]]
                cuotas = promoObj.cuotas
                total = this.total * promoObj.coeficiente

                if (!final && promoObj.pre)
                    total = promoObj.pre(total)
            }
            var cuota = total / cuotas
            return {
                total: Math.floor(total * 100) / 100,
                cuota: Math.floor(cuota * 100) / 100,
                ncuotas: cuotas
            }
        },
        siguientePaso: function() {
            var self = this

            var descripcion = this.pedido.items.reduce(function(acc, item) {
                return acc + item.descripcion + " + "
            }, "").slice(0, -3)

            var intereses = this.calcularInteres(this.pedido.tarjeta, this.formasDePago[this.pedido.metodo], true)

            var tarjetaId = this.pedido.tarjeta

            var cuotas = intereses.ncuotas

            var promo
            if (this.formasDePago[this.pedido.metodo].promocion && this.formasDePago[this.pedido.metodo].promocion[tarjetaId])
                    promo = this.formasDePago[this.pedido.metodo].promocion[tarjetaId]

            if (tarjetaId === 'visa12' && promo === 'promoahora12') {
                cuotas = 7
                promo = undefined
            }
            
            if (tarjetaId === 'visa18' && promo === 'promoahora18') {
                cuotas = 8
                promo = undefined
            }

            var obj = {
                metodo: this.tarjetas[tarjetaId].codigo,
                titular: {
                    email: this.pedido.titular.correo
                },
                total: intereses.total * 100,
                cuotas: cuotas,
                descripcion: descripcion,
                id: this.id,
		promo: promo
            }

            

            this.cargando = true;

            axios.post('http://198.38.86.180/~neumaticosverona/index.php?route=extension/payment/nps/pay', obj)
                .then(function(response) {

                    console.log("----");
                    console.log("response.data", response.data);
                    console.log("----");
                    self.nps.action = response.data.action
                    self.nps.session = response.data.session
                    self.nps.txRef = response.data.txRef
                    self.nps.merchantId = response.data.merchantId
                    self.nps.transactionId = response.data.transactionId
                    self.nps.name = 'formnps'
                    self.nps.buttonId = 'btnnps'


                    var interval = setInterval(function() {
                        //document.forms['formnps'].submit()
                        var btn = document.getElementById('btnnps')
                        if (btn) {
                            clearInterval(interval)
                            btn.click()
                        }
                    }, 10)


                    /*if (response.data.action) {
                    } else {
                      self.mostrarModal('Error', response.data, function () {
                        location.href = '/nps/'
                      }, function () {
                        location.href = '/nps/'	
                      })
                    }*/

                }).catch(function(response) {
                    console.log('ERROR', response)
                })
        }
    },
    computed: {
        advertencia: function() {
            var tarjetaId = this.pedido.tarjeta
            var metodo = this.formasDePago[this.pedido.metodo]
            var tarjetaObj = this.tarjetas[tarjetaId]

            if (metodo.promocion && metodo.promocion[tarjetaId]) {
                var promoObj = tarjetaObj.promociones[metodo.promocion[tarjetaId]]
                if (promoObj.advertencia)
                    return promoObj.advertencia
            }
            return false
        },
        formasDePago: function() {
            return [{
                    strong: '18',
                    text: 'PAGOS',
                    promocion: {
                        visa18: 'promoahora18'
                    },
                    labels: ['Sin interés', 'AHORA 18'],
                    tarjetas: ['visa18']
                },{
                    strong: '12',
                    text: 'PAGOS',
                    promocion: {
                        visa12: 'promoahora12'
                    },
                    labels: ['Sin interés', 'AHORA 12'],
                    tarjetas: ['visa12']
                },
                {
                    strong: '24',
                    text: 'CUOTAS DE $' + Math.floor(this.total / 24 * 100) / 100,
                    cuotas: 24,
                    labels: ['Sin interés'],
                    tarjetas: ['naranja']
                },
                {
                    strong: '18',
                    text: 'CUOTAS DE $' + Math.floor(this.total / 18 * 100) / 100,
                    cuotas: 18,
                    labels: ['Sin interés'],
                    tarjetas: ['naranja']
                },
                {
                    strong: '14',
                    text: 'CUOTAS DE $' + Math.floor(this.total / 14 * 100) / 100,
                    cuotas: 14,
                    labels: ['Sin interés'],
                    tarjetas: ['naranja']
                },{
                    strong: '12',
                    text: 'CUOTAS DE $' + Math.floor(this.total / 12 * 100) / 100,
                    cuotas: 12,
                    labels: ['Sin interés'],
                    tarjetas: ['naranja']
                },
                {
                    strong: '6',
                    text: 'PAGOS',
                    labels: ['Sin interés', '15% reintegro'],
                    promocion: {
                        nbsf: 'promosantafe'
                    },
                    tarjetas: ['nbsf']
                },
                /*{
                    strong: '6',
                    text: 'PAGOS',
                    labels: ['Sin interés', '10% reintegro'],
                    promocion: {
                        //hsbc: 'promohsbc',
                        //cabal: 'promocabal'
                    },
                    tarjetas: ['hsbc', 'cabal']
                },
                {
                    strong: '3',
                    text: 'PAGOS',
                    labels: ['Sin interés', '10% reintegro'],
                    promocion: {
                        //bmros: 'promomunicipal'
                    },
                    tarjetas: ['bmros']
                },*/
                {
                    strong: '1',
                    cuotas: 1,
                    text: 'PAGO',
                    tarjetas: ['visa', 'hsbc', 'cabal', 'bmros', 'nbsf'/*, 'naranja'*/]
                },
                {
                    strong: '3',
                    cuotas: 3,
                    text: 'CUOTAS DE $' + Math.floor(this.total / 3 * 100) / 100,
                    labels: ['Sin Interés'],
                    tarjetas: ['visa', 'hsbc', 'cabal', 'bmros', 'nbsf']
                },
                {
                    strong: '6',
                    cuotas: 6,
                    text: 'CUOTAS DE $' + Math.floor(this.total / 6 * 100) / 100,
                    labels: ['Sin Interés'],
                    tarjetas: ['visa', 'hsbc', 'cabal', 'bmros', 'nbsf'/*, 'naranja'*/]
                },
                {
                    strong: 'DEBITO',
                    cuotas: 1,
                    text: 'VISA',
                    tarjetas: ['debito']
                }

            ]
        },
        cuotasDisponibles: function() {
            if (this.pedido.metodo.codigo > 100)
                return undefined
            if (this.pedido.metodo.codigo === 14) {
                return [{
                        codigo: 1,
                        descripcion: "Un pago",
                        interes: 1
                    },
                    {
                        codigo: 2,
                        descripcion: "Dos pagos",
                        interes: 1.0504
                    },
                    {
                        codigo: 3,
                        descripcion: "Tres pagos",
                        interes: 1.0684
                    },
                    {
                        codigo: 6,
                        descripcion: "Seis pagos",
                        interes: 1.1235
                    },
                    {
                        codigo: 12,
                        descripcion: "Doce pagos",
                        interes: 1.2632
                    }
                ]
            }
        },
        nombreCompletado: function() {
            if (this.pedido.titular.nombre.length > 5)
                return true
            return false
        },
        dniCompletado: function() {
            if (this.pedido.titular.dni.length > 6)
                return true
            return false
        },
        correoCompletado: function() {
            if (this.pedido.titular.correo.length > 7 && this.pedido.titular.correo.indexOf('@') != -1)
                return true
            return false
        },
        datosCompletados: function() {
            if (this.nombreCompletado && this.dniCompletado && this.correoCompletado)
                return true
            return false
        },
        total: function() {
            if (!this.pedido.items)
                return 0
            return this.pedido.items.reduce(function(acc, val) {
                return acc + val.precio
            }, 0)
        }
    },
    mounted() {
        var self = this
        axios.get('http://198.38.86.180/~neumaticosverona/index.php?route=extension/payment/nps/getData')
            .then(function(response) {
                self.pedido.items = response.data.items || []
                self.pedido.titular.correo = response.data.email || ''
                self.id = response.data.id || 0
            })
            .catch(function(response) {
                console.log('ERROR', response)
            })
    }
})
