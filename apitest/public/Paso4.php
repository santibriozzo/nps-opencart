<?php

if(empty($_SERVER['HTTP_REFERER'])) header("Location: Paso1.php");
if(substr($_SERVER['HTTP_REFERER'],-5,1) != 3) header("Location: Paso3.php");

session_start();

require_once(dirname(__FILE__).'/../lib/Sub1/psp_client.php');
require_once(dirname(__FILE__).'/../lib/Sub1/psp_log.php');
require_once(dirname(__FILE__).'/../lib/Sub1/functions.php');

$config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');

psp_log('PASO4: La aplicacion envia los datos al servidor NPS para poder realizar el pago');

?>
<html>
<head>
<title>NPS :: MODELO 3 PARTES :: PASO 4</title>    
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<h1>MODELO 3 PARTES (<?php echo isset($_SESSION['server']) ? $_SESSION['server'] : 'default' ?>)</h1>

<?php


/*
 * Request Parameters
 */
$_SESSION['psp_NumPayments'] = isset($_REQUEST['psp_NumPayments']) ? $_REQUEST['psp_NumPayments'] : '';
$_SESSION['psp_FirstExpDate'] = isset($_REQUEST['psp_FirstExpDate']) ? $_REQUEST['psp_FirstExpDate'] : '';
$_SESSION['secondExpDate'] = isset($_REQUEST['secondExpDate']) ? $_REQUEST['secondExpDate'] : '';
$_SESSION['psp_SurchargeAmount'] = isset($_REQUEST['psp_SurchargeAmount']) ? $_REQUEST['psp_SurchargeAmount'] : '';
$_SESSION['psp_VisaArg_DA_DocType'] = isset($_REQUEST['psp_VisaArg_DA_DocType']) ? $_REQUEST['psp_VisaArg_DA_DocType'] : '';
$_SESSION['psp_VisaArg_DA_DocNum'] = isset($_REQUEST['psp_VisaArg_DA_DocNum']) ? $_REQUEST['psp_VisaArg_DA_DocNum'] : '';
$_SESSION['psp_VisaArg_DA_DoorNum'] = isset($_REQUEST['psp_VisaArg_DA_DoorNum']) ? $_REQUEST['psp_VisaArg_DA_DoorNum'] : '';
$_SESSION['psp_VisaArg_DA_BirthDay'] = isset($_REQUEST['psp_VisaArg_DA_BirthDay']) ? $_REQUEST['psp_VisaArg_DA_BirthDay'] : '';
$_SESSION['psp_VisaArg_DA_Name'] = isset($_REQUEST['psp_VisaArg_DA_Name']) ? $_REQUEST['psp_VisaArg_DA_Name'] : '';
$_SESSION['psp_CustomerDocNum'] = isset($_REQUEST['psp_CustomerDocNum']) ? $_REQUEST['psp_CustomerDocNum'] : '';
$_SESSION['psp_CustomerFirstName'] = isset($_REQUEST['psp_CustomerFirstName']) ? $_REQUEST['psp_CustomerFirstName'] : '';
$_SESSION['psp_CustomerLastName'] = isset($_REQUEST['psp_CustomerLastName']) ? $_REQUEST['psp_CustomerLastName'] : '';
$_SESSION['psp_CustomerAddress'] = isset($_REQUEST['psp_CustomerAddress']) ? $_REQUEST['psp_CustomerAddress'] : '';
$_SESSION['psp_CustomerCountry'] = isset($_REQUEST['psp_CustomerCountry']) ? $_REQUEST['psp_CustomerCountry'] : '';
$_SESSION['psp_CustomerProvince'] = isset($_REQUEST['psp_CustomerProvince']) ? $_REQUEST['psp_CustomerProvince'] : '';
$_SESSION['psp_CustomerLocality'] = isset($_REQUEST['psp_CustomerLocality']) ? $_REQUEST['psp_CustomerLocality'] : '';
$_SESSION['psp_MasterArg_AVS_BirthDay'] = isset($_REQUEST['psp_MasterArg_AVS_BirthDay']) ? $_REQUEST['psp_MasterArg_AVS_BirthDay'] : '';
$_SESSION['psp_MasterArg_AVS_AdditionalsQtty'] = isset($_REQUEST['psp_MasterArg_AVS_AdditionalsQtty']) ? $_REQUEST['psp_MasterArg_AVS_AdditionalsQtty'] : '';
$_SESSION['psp_MasterArg_AVS_PostalCode'] = isset($_REQUEST['psp_MasterArg_AVS_PostalCode']) ? $_REQUEST['psp_MasterArg_AVS_PostalCode'] : '';
$_SESSION['psp_MasterArg_AVS_BranchEntity'] = isset($_REQUEST['psp_MasterArg_AVS_BranchEntity']) ? $_REQUEST['psp_MasterArg_AVS_BranchEntity'] : '';
$_SESSION['psp_AmexArg_AVS_Address'] = isset($_REQUEST['psp_AmexArg_AVS_Address']) ? $_REQUEST['psp_AmexArg_AVS_Address'] : '';
$_SESSION['psp_AmexArg_AVS_PostCode'] = isset($_REQUEST['psp_AmexArg_AVS_PostCode']) ? $_REQUEST['psp_AmexArg_AVS_PostCode'] : '';
$_SESSION['psp_TicketDescription'] = isset($_REQUEST['psp_TicketDescription']) ? $_REQUEST['psp_TicketDescription'] : '';
$_SESSION['psp_ExpDate1'] = isset($_REQUEST['psp_ExpDate1']) ? $_REQUEST['psp_ExpDate1'] : '';
$_SESSION['psp_Amount1'] = isset($_REQUEST['psp_Amount1']) ? $_REQUEST['psp_Amount1'] : '';
$_SESSION['psp_ExpDate2'] = isset($_REQUEST['psp_ExpDate2']) ? $_REQUEST['psp_ExpDate2'] : '';
$_SESSION['psp_Amount2'] = isset($_REQUEST['psp_Amount2']) ? $_REQUEST['psp_Amount2'] : '';
$_SESSION['psp_ExpDate3'] = isset($_REQUEST['psp_ExpDate3']) ? $_REQUEST['psp_ExpDate3'] : '';
$_SESSION['psp_Amount3'] = isset($_REQUEST['psp_Amount3']) ? $_REQUEST['psp_Amount3'] : '';
$_SESSION['psp_MinAmount'] = isset($_REQUEST['psp_MinAmount']) ? $_REQUEST['psp_MinAmount'] : '';
$_SESSION['psp_ExpTime'] = isset($_REQUEST['psp_ExpTime']) ? $_REQUEST['psp_ExpTime'] : '';




$method_name = getPSPMethodByPSPProduct($_SESSION['psp_Product']);



/*
 * Basic Parameters
 */
$psp_parameters = array(
    'psp_Version'                  => $config['psp_Version'],
    'psp_MerchantId'               => $_SESSION['psp_MerchantId'],
    'psp_TxSource'                 => $config['psp_TxSource'],
    'psp_MerchTxRef'               => rand(200,10000000), // unico
    'psp_PosDateTime'              => date('Y-m-d H:i:s'),
    'psp_MerchOrderId'             => rand(200,10000000), // unico por intento de transaccion
    'psp_Currency'                 => $_SESSION['psp_Currency'],
    'psp_CustomerMail'             => $_SESSION['psp_CustomerMail'],
    'psp_MerchantMail'             => $_SESSION['psp_MerchantMail'],
    'psp_Product'                  => $_SESSION['psp_Product'],
    'psp_Country'                  => $_SESSION['psp_Country'],
    'psp_ReturnURL'                => $config['psp_ReturnURL'],
    'psp_FrmLanguage'              => $config['psp_FrmLanguage'],
    'psp_FrmBackButtonURL'         => $config['psp_FrmBackButtonURL'],
);

/*
 * Aditionals Parameters
 */
switch($method_name){
    case'CashPayment_3p':
        $psp_parameters['psp_FirstExpDate']=$_SESSION['psp_FirstExpDate'];
        $psp_parameters['psp_DaysUntilSecondExpDate']=calculateDaysUntilSecondExpDate();
        $psp_parameters['psp_SurchargeAmount']=$_SESSION['psp_SurchargeAmount'];
        $psp_parameters['psp_DaysAvailableToPay']=calculateDaysAvailableToPay();
        $psp_parameters['psp_CustomerDocNum']=$_SESSION['psp_CustomerDocNum'];
        $psp_parameters['psp_CustomerFirstName']=$_SESSION['psp_CustomerFirstName'];
        $psp_parameters['psp_CustomerLastName']=$_SESSION['psp_CustomerLastName'];
        $psp_parameters['psp_CustomerAddress']=$_SESSION['psp_CustomerAddress'];
        $psp_parameters['psp_CustomerCountry']=$_SESSION['psp_CustomerCountry'];
        $psp_parameters['psp_CustomerProvince']=$_SESSION['psp_CustomerProvince'];
        $psp_parameters['psp_CustomerLocality']=$_SESSION['psp_CustomerLocality'];        
        $psp_parameters['psp_Amount']=$_SESSION['psp_Amount'];
        $psp_parameters['psp_PurchaseDescription']=$_SESSION['psp_PurchaseDescription'];
        break;
    case 'PayOnLine_3p':
        // totalAmoun with psp Format $$$$cc
        $psp_parameters['psp_Amount'] = getPSPAmount($_SESSION['psp_NumPayments'], $_SESSION['psp_Product'], $_SESSION['psp_Amount']);
        $psp_parameters['psp_NumPayments'] = $_SESSION['psp_NumPayments'];
        $psp_parameters['psp_PurchaseDescription']=$_SESSION['psp_PurchaseDescription'];
        
        
        if($_SESSION['psp_Product'] == 14) { //AVS VISA
            $psp_parameters['psp_VisaArg_DA_DocType']=$_SESSION['psp_VisaArg_DA_DocType'];
            $psp_parameters['psp_VisaArg_DA_DocNum']=$_SESSION['psp_VisaArg_DA_DocNum'];
            $psp_parameters['psp_VisaArg_DA_DoorNum']=$_SESSION['psp_VisaArg_DA_DoorNum'];
            $psp_parameters['psp_VisaArg_DA_BirthDay']=$_SESSION['psp_VisaArg_DA_BirthDay'];
            $psp_parameters['psp_VisaArg_DA_Name']=$_SESSION['psp_VisaArg_DA_Name'];
        }
        if($_SESSION['psp_Product'] == 5) { //AVS MASTERCARD
            $psp_parameters['psp_MasterArg_AVS_BirthDay']=$_SESSION['psp_MasterArg_AVS_BirthDay'];
            $psp_parameters['psp_MasterArg_AVS_AdditionalsQtty']=$_SESSION['psp_MasterArg_AVS_AdditionalsQtty'];
            $psp_parameters['psp_MasterArg_AVS_PostalCode']=$_SESSION['psp_MasterArg_AVS_PostalCode'];
            $psp_parameters['psp_MasterArg_AVS_BranchEntity']=$_SESSION['psp_MasterArg_AVS_BranchEntity'];
        }
        if($_SESSION['psp_Product'] == 1) { //AVS AMEX
            $psp_parameters['psp_AmexArg_AVS_Address']=$_SESSION['psp_AmexArg_AVS_Address'];
            $psp_parameters['psp_AmexArg_AVS_PostCode']=$_SESSION['psp_AmexArg_AVS_PostCode'];
        }        
        break;
    case 'BankPayment_3p':    
        $psp_parameters['psp_ScreenDescription']=$config['psp_ScreenDescription'];
        $psp_parameters['psp_TicketDescription']=$_SESSION['psp_TicketDescription'];
        $psp_parameters['psp_ExpDate1']=$_SESSION['psp_ExpDate1'];
        $psp_parameters['psp_Amount1']=$_SESSION['psp_Amount1'];
        $psp_parameters['psp_ExpDate2']=$_SESSION['psp_ExpDate2'];
        $psp_parameters['psp_Amount2']=$_SESSION['psp_Amount2'];
        $psp_parameters['psp_ExpDate3']=$_SESSION['psp_ExpDate3'];
        $psp_parameters['psp_Amount3']=$_SESSION['psp_Amount3'];
        $psp_parameters['psp_MinAmount']=$_SESSION['psp_MinAmount'];
        $psp_parameters['psp_ExpTime']=$_SESSION['psp_ExpTime'];
        $psp_parameters['psp_ExpMark']=$config['psp_ExpMark'];
        break;
}

try
{
  $cli = new PSP_Client();
  $cli->setDebug(false);
  $cli->setPrintRequest(false);
  $cli->setPrintResponse(false);
  $cli->setConnectTimeout(20);
  $cli->setExecuteTimeout(40);
  
  $cli->setUrl($config['ws_url']);

  $cli->setWsdlCache('../cache/', 0);
  
  $cli->setSecretKey($_SESSION['secret_key']);
  
   
  $cli->setMethodName($method_name);
  $cli->setMethodParams($psp_parameters);
  $result = $cli->send();

  psp_log('PASO4: se envian los parametros al metodo ('.$method_name.')');

  // Submit 3p Form
  if (isset($result['psp_Session3p']) && $result['psp_Session3p'] != '') {
    echo "<h2>4. El servidor PSP autoriz&oacute; la transacci&oacute;n, y el sitio web se dispone a enviar los siguientes datos para ir al formulario de pago</h2>";
    echo render3pForm($result);

    psp_log('PASO4: transaccion aprobada');
    
  }else {
    echo "<h2>4. El servidor PSP no autoriz&oacute; la transacci&oacute;n, por lo tanto sitio web no puede continuar al formulario de pago</h2>";
    echo "<pre>";
    echo '<h2>Result</h2><pre>';
    print_r($result);
    echo "</pre>";

      psp_log('La transaccion no fue aprobada ('.$result.')');
    
  }  
  
}
catch (Exception $e)
{
  psp_log('ERROR '.$e->getCode().' - '.$e->getMessage());
    
  echo '<h2>Result</h2><pre>';
  echo 'Error Cod: '.$e->getCode().'<br>';
  echo 'Error Msg: '.$e->getMessage().'<br>';
  echo '</pre>';
}

?>
</body>
</html>