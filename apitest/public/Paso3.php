<?php

if(empty($_SERVER['HTTP_REFERER'])) header("Location: Paso1.php");
if(substr($_SERVER['HTTP_REFERER'],-5,1) != 2) header("Location: Paso2.php");

session_start();
include_once(dirname(__FILE__).'/../lib/Sub1/functions.php');
require_once(dirname(__FILE__).'/../lib/Sub1/psp_log.php');
$config = getConfigsByServerName(isset($_SESSION['server']) ? $_SESSION['server'] : 'default');

$_SESSION['psp_Product'] = isset($_REQUEST['psp_Product']) ? $_REQUEST['psp_Product'] : '';

$fees = getFeesByPSPProduct($_SESSION['psp_Product']);

psp_log('PASO3: El usuario selecciono el metodo de pago ('.$_SESSION['psp_Product'].') y completa los datos faltantes');

?>
<html>
<head>
<title>NPS :: MODELO 3 PARTES :: PASO 3 </title>    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script>
function showTotalAmount(){

    var xmlhttp;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("total_amount").innerHTML=xmlhttp.responseText;

        }
      }
    xmlhttp.open("GET","ajax_fees.php?psp_NumPayments="+document.getElementById('psp_NumPayments').options[document.getElementById('psp_NumPayments').selectedIndex].value,true);
    xmlhttp.send();

}
</script>
    
    
</head>    
<body>
<h1>MODELO 3 PARTES (<?php echo isset($_SESSION['server']) ? $_SESSION['server'] : 'default' ?>)</h1>

<h2>3. El usuario completa los datos faltantes y la p&aacute;gina web proceder&aacute; a conectarse con el servidor NPS para pedir autorizaci&oacute;n para el medio de pago seleccionado</h2>

<table border="1" cellpadding="0" cellspacing="0" height="50">
    <tr>
        <td>TOTAL A PAGAR:</td>
        <td>$<span id="total_amount"><?php echo number_format($_SESSION['psp_Amount']/100,2) ?></span></td>
    </tr>
</table>

<form action="Paso4.php" method="POST">
<input type="hidden" name="psp_Amount" id="psp_Amount" value="<?php echo $_SESSION['psp_Amount'] ?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <?php if($_SESSION['psp_Product'] == 14 || $_SESSION['psp_Product'] == 5 || $_SESSION['psp_Product'] == 1 || $_SESSION['psp_Product'] == 2): ?>
    <!--[PAGO CON TARJETA]-->
    <tr>
        <td>Cantidad de cuotas</td>
        <td>
            <?php if($fees): ?>
            <select name="psp_NumPayments" id="psp_NumPayments" onchange="showTotalAmount()">
                <?php foreach(getFeesByPSPProduct(14) as $fee): ?>
                <option value="<?php echo $fee['quantity'] ?>"><?php echo $fee['quantity'] ?></option>
                <?php endforeach ?>
            </select>
            <a href="fees.php?psp_product=<?php echo $_SESSION['psp_Product']?>&psp_amount=<?php echo $_SESSION['psp_Amount'] ?>" target="_blank">ver cuotas</a>
            <?php else: ?>
            <span style="color:red;">Falta configurar las cuotas en el archivo card_interest.xml</a>
            <script>
                window.onload = function() { document.getElementById('continuar').disabled = true; }
                
            </script>
            <?php endif ?>
        </td>
    </tr>
    <?php if($_SESSION['psp_Product'] == 14): ?>
    <tr>
      <td colspan="2"><br/><b>Datos adicionales (VISA)</b></td>
    </tr>    
    <tr>
        <td>Tipo de documento</td>
        <td>
            <select name="psp_VisaArg_DA_DocType">
                <option value="1">DNI</option>
                <option value="2">CI</option>
                <option value="3">LE</option>
                <option value="4">LC</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Documento N&uacute;mero</td>
        <td>
            <input type="text" name="psp_VisaArg_DA_DocNum" value="29658965" />
        </td>
    </tr>
    <tr>
        <td>Direcci&oacute;n N&uacute;mero</td>
        <td>
            <input type="text" name="psp_VisaArg_DA_DoorNum" value="854" />*donde recibe el resumen
        </td>
    </tr>
    <tr>
        <td>Fecha de Nacimiento</td>
        <td>
            <input type="text" name="psp_VisaArg_DA_BirthDay" value="10021981" />
        </td>
    </tr>
    <tr>
        <td>Nombre Completo</td>
        <td>
            <input type="text" name="psp_VisaArg_DA_Name" value="José" />*como figura en la tarjeta
        </td>
    </tr>    
    <?php endif ?>
    <?php if($_SESSION['psp_Product'] == 5): ?>
    <tr>
      <td colspan="2"><br/><b>Datos adicionales (MC)</b></td>
    </tr>
    <tr>
        <td>Fecha de Nacimiento</td>
        <td>
            <input type="text" name="psp_MasterArg_AVS_BirthDay" value="12011979" />
        </td>
    </tr>
    <tr>
        <td>Cantidad de adicionales</td>
        <td>
            <input type="text" name="psp_MasterArg_AVS_AdditionalsQtty" value="1" />
        </td>
    </tr>
    <tr>
        <td>Código Postal</td>
        <td>
            <input type="text" name="psp_MasterArg_AVS_PostalCode" value="1414" />*donde recibe el resumen
        </td>
    </tr>
    <tr>
        <td>Entidad sucursal</td>
        <td>
            <input type="text" name="psp_MasterArg_AVS_BranchEntity" value="003265" />*últimos 6 dígitos al frente de la tarjeta de la última línea
        </td>
    </tr>    
    <?php endif ?>
    <?php if($_SESSION['psp_Product'] == 1): ?>
    <tr>
      <td colspan="2"><br/><b>Datos adicionales (AMEX)</b></td>
    </tr>    
    <tr>
        <td>Dirección</td>
        <td>
            <input type="text" name="psp_AmexArg_AVS_Address" value="Balcarce 50" />
        </td>
    </tr>    
    <tr>
        <td>Código Postal</td>
        <td>
            <input type="text" name="psp_AmexArg_AVS_PostCode" value="1411" />
        </td>
    </tr>    
    <?php endif ?>
    <?php endif ?>
    <?php if($_SESSION['psp_Product'] == 302 || $_SESSION['psp_Product'] == 301 || $_SESSION['psp_Product'] == 300): ?>
    <!--[PAGO CON CASH]-->
    <tr>
        <td>Fecha 1er Vencimiento</td>
        <td>
            <input type="text" value="<?php echo date('Y-m-d',strtotime("+1 month")) ?>" name="psp_FirstExpDate"  />
        </td>
    </tr>    
    <tr>
        <td>Fecha 2do Vencimiento</td>
        <td>
            <input type="text" value="<?php echo date('Y-m-d',strtotime("+2 month")) ?>" name="SecondExpDate"  />
        </td>
    </tr>        
    <tr>
        <td>Recargo</td>
        <td>
            <input type="text" value="1000" name="psp_SurchargeAmount"  />[$$$$$cc]
        </td>
    </tr>
    <tr>
        <td>Documento número</td>
        <td>
            <input type="text" value="27087764" name="psp_CustomerDocNum" />
        </td>
    </tr>
    <tr>
        <td>Nombre</td>
        <td>
            <input type="text" value="Juan" name="psp_CustomerFirstName" />
        </td>
    </tr>
    <tr>
        <td>Apellido</td>
        <td>
            <input type="text" value="Perez" name="psp_CustomerLastName" />
        </td>
    </tr>
    <tr>
        <td>Dirección</td>
        <td>
            <input type="text" value="Rivadavia 50" name="psp_CustomerAddress" />
        </td>
    </tr>
    <tr>
        <td>País</td>
        <td>
            <input type="text" value="Argentina" name="psp_CustomerCountry" />
        </td>
    </tr>
    <tr>
        <td>Provincia</td>
        <td>
            <input type="text" value="Cap. Fed." name="psp_CustomerProvince" />
        </td>
    </tr>
    <tr>
        <td>Localidad</td>
        <td>
            <input type="text" value="Cap. Fed." name="psp_CustomerLocality" />
        </td>
    </tr>    
    <?php endif ?>
    <?php if($_SESSION['psp_Product'] == 320): ?>
    <tr>
        <td>Descripción</td>
        <td>
            <input type="text" value="Descripcion de Ticket" name="psp_TicketDescription" />
        </td>
    </tr>
    <tr>
        <td>Vencimiento 1</td>
        <td>
            <input type="text" value="<?php echo date('Y-m-d',strtotime("+1 month")) ?>" name="psp_ExpDate1" />[aaaa-mm-dd]
        </td>
    </tr>    
    <tr>
        <td>Monto 1</td>
        <td>
            $<input type="text" value="1200" name="psp_Amount1" />[$$$$cc]
        </td>
    </tr>
    <tr>
        <td>Vencimiento 2</td>
        <td>
            <input type="text" value="<?php echo date('Y-m-d',strtotime("+2 month")) ?>" name="psp_ExpDate2" />[aaaa-mm-dd]
        </td>
    </tr>    
    <tr>
        <td>Monto 2</td>
        <td>
            $<input type="text" value="100" name="psp_Amount2" />[$$$$cc]
        </td>
    </tr>
    <tr>
        <td>Vencimiento 3</td>
        <td>
            <input type="text" value="<?php echo date('Y-m-d',strtotime("+3 month")) ?>" name="psp_ExpDate3" />[aaaa-mm-dd]
        </td>
    </tr>    
    <tr>
        <td>Monto 3</td>
        <td>
            $<input type="text" value="100" name="psp_Amount3" />[$$$$cc]
        </td>
    </tr>
    <tr>
        <td>Pago minimo</td>
        <td>
            <input type="text" value="100" name="psp_MinAmount" />[$$$$cc]
        </td>
    </tr>
    <tr>
        <td>Hora de vencimiento</td>
        <td>
            <input type="text" value="13:00:00" name="psp_ExpTime" />[hh:mm:ss]
        </td>
    </tr>    
    <?php endif ?>
</table>
<br />
<input type="submit" value="continuar" id="continuar"/>
</form>
</body>
</html>
